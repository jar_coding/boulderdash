/**
 * Top-Level Library
 * @description: Methoden und Konstanten auf TopLevel Ebene. 
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
library boulderLib;

import 'dart:html';
import 'dart:convert';
import 'dart:async';
import 'dart:math';

/* Entities */
part 'src/entities/Amoeba.dart';
part 'src/entities/Boulder.dart';
part 'src/entities/Brickwall.dart';
part 'src/entities/Butterfly.dart';
part 'src/entities/Diamond.dart';
part 'src/entities/Dirt.dart';
part 'src/entities/Explode.dart';
part 'src/entities/Firefly.dart';
part 'src/entities/Magicwall.dart';
part 'src/entities/Metal.dart';
part 'src/entities/Outbox.dart';
part 'src/entities/Rockford.dart';
part 'src/entities/Space.dart';
/* Abstracts Entities */
part 'src/abstracts/Entity.dart';
part 'src/abstracts/ExplodableEntity.dart';
part 'src/abstracts/MoveableEntity.dart';
/* Interfaces */
part 'src/interfaces/IExplodable.dart';
part 'src/interfaces/IMoveable.dart';
part 'src/interfaces/IUpdateable.dart';

part 'src/Field.dart';

/* MVC */
part 'src/Model.dart';
part 'src/View.dart';
part 'src/ViewInterface.dart';
part 'src/Controller.dart';

/**
 * Meta Daten
 */ 
const BLOCK_SIZE          = 32;
const DIR_COUNT           = 8;
const SEC_TO_START        = 0;
const PERIODIC_IN_MS      = 30;
const LIFE_AT_THE_BEGIN   = 3;
const DIAMOND_VALUE       = 10;
const EXTRA_DIAMOND_VALUE = 15;

/**
 * Mögliche Richtungen
 */ 
const int UP            =  0;
const int UPRIGHT       =  1;
const int RIGHT         =  2;
const int DOWNRIGHT     =  3;
const int DOWN          =  4;
const int DOWNLEFT      =  5;
const int LEFT          =  6;
const int UPLEFT        =  7;
const int NONE          =  8;
/**
 * Entity Typen
 */ 
const int SPACE               =  0;
const int DIRT                =  1;
const int BOULDER             =  2;
const int METAL               =  3;
const int BRICKWALL           =  4;
const int DIAMOND             =  5;
const int ROCKFORD            =  6;
const int ROCKFORD_LEFT       =  7;
const int ROCKFORD_RIGHT      =  8;
const int MAGICWALL           =  9;
const int MAGICWALL_ACTIVE    = 10;
const int FIREFLY             = 11;
const int BUTTERFLY           = 12;
const int AMOEBA              = 13;
const int OUTBOX              = 14;
const int OUTBOX_ACTIVE       = 15;
const int EXPLODE             = 16;

/**
 * Map um den Typ einer [Entity] als String mit der ID zu verknüpfen.  
 */ 
Map<String,num> typeMap = {
  "space"             :  0,
  "dirt"              :  1,
  "boulder"           :  2,
  "metal"             :  3,
  "brickwall"         :  4,
  "diamond"           :  5,
  "rockford"          :  6,
  "rockford_left"     :  7,
  "rockford_right"    :  8,
  "magicwall"         :  9,
  "magicwall_active"  : 10,
  "firefly"           : 11,
  "butterfly"         : 12,
  "amoeba"            : 13,
  "outbox"            : 14,
  "outbox_active"     : 15,
  "explode"           : 16                  
};
/**
 * Liste der möglichen delta X Werte einer Richtung
 */ 
var DIRX = [      0,            1,          1,              1,         0,            -1,        -1,          -1,         0 ];
/**
 * Liste der möglichen delta Y Werte einer Richtung
 */ 
var DIRY = [     -1,           -1,          0,              1,         1,             1,         0,          -1,         0 ];
//         {'UP': 0, 'UPRIGHT': 1, 'RIGHT': 2, 'DOWNRIGHT': 3, 'DOWN': 4, 'DOWNLEFT': 5, 'LEFT': 6, 'UPLEFT': 7, 'NONE': 8 }
/**
 * Prüft ob die übergebene Richtung horizontal verläuft
 * @param: [num] dir: Zu prüfende Richtung
 * @return true/false
 */ 
var horizontal = (num dir) => DIRX[dir] != 0;
/**
 * Prüft ob die übergebene Richtung vertikal verläuft
 * @param: [num] dir: Zu prüfende Richtung
 * @return true/false
 */ 
var vertical   = (num dir) => DIRY[dir] != 0;
/**
 * Liste aller möglichen Laufrichtungen
 */ 
var moveableDir = [UP, RIGHT, DOWN, LEFT];
/**
 * Gibt anhand einer Richtung [num] die Richtung nach einer Linksrotation zurück
 * @param: [num] dir: aktuelle Richtung
 * @return Richtung [num] nach Linksrotation
 */ 
num rotateLeft(num dir)   => (dir-2) + (dir < 2 ? 8 : 0);
/**
 * Gibt anhand einer Richtung [num] die Richtung nach einer Rechtsrotation zurück
 * @param: [num] dir: aktuelle Richtung
 * @return Richtung [num] nach Rechtsrotation
 */ 
num rotateRight(num dir)  => (dir+2) - (dir > 5 ? 8 : 0);
/**
 * Referenz auf einen [NodeValidatorBuilder] der es den <td> Elementen erlaubt die Attribute x und y zu verwenden
 */ 
final NodeValidatorBuilder _htmlValidator = new NodeValidatorBuilder.common()
..allowElement('td', attributes: ['x', 'y']);
/**
 * Map mit den configinformationen
 */ 
Map<String,List<String>> config = new Map<String,List<String>>();
/**
 * Prüft ob eine [Entity] lebendig ist
 * @param: [Entity] e: Zu prüfende [Entity]
 * @return true/false
 */ 
bool isLivingEntity(Entity e) {
  return e.type == BUTTERFLY || e.type == FIREFLY || e.type == ROCKFORD;
}
/**
 * Prüft ob eine [Entity] abgerundet ist
 * @param: [Entity] e: Zu prüfende [Entity]
 * return true/false
 */ 
bool isRounded(Entity e) {
  return e.type == BRICKWALL || e.type == BOULDER || e.type == DIAMOND;
}