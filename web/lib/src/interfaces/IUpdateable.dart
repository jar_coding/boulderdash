part of boulderLib;
/**
 * IUpdateable Interface
 * @description: Interface welches Objekte Implementieren können,
 * welche in einem bestimmten Intervall ein update ausführen.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
abstract class IUpdateable {
  
  /**
   * Aktuelles Frame welches durchlaufen wird
   */
  num _frame = 0;
  /**
   * Maxiamle Frames die vorrübergehen dürfen bis das Objekt ein Update ausführt.
   */
  num _maxFrame;
  
  /**
   * Prüft ob ein Objekt in diesem Intervall eine Aktion ausführen kann
   * @return: true - Objekt kann eine Aktion ausführen, sonst false
   */ 
  bool _isReady();
  /**
   * Update wird abhängig von _maxFrame eines jeden Objektes periodisch ausgeführt
   * Und bestimmt das verhalten des Objektes
   */ 
  void update();
}