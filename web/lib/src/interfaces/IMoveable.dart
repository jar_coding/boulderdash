part of boulderLib;
/**
 * IMoveable Interface
 * @description: Interface welches Objekte Implementieren können,
 * welche bewegbar sein sollen.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
abstract class IMoveable {
  /**
   * Gibt die letzte gespeicherte Richtung an
   */
  num _lastDir;
  /**
   * Gibt an ob das Objekt gerade in Bewegung ist
   */
  bool _isMoving;
  
  /**
   * Prüft ob das Objekt von allein Seiten aus Blockiert ist,
   * das heißt wenn es sich in keine freie Richtung bewegen kann.
   * @return: true - wenn es Blockiert ist, sonst false
   */
  bool get isBlocked;
  /**
   * Bewegt eine [Entity] in eine bestimmte freie Richtung
   * @params: dir: Richtung
   * @return: true, wenn die Bewegung erfolgreich war,
   * andernfalls false;
   */
  bool move(num dir);
}