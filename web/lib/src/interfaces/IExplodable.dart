part of boulderLib;
/**
 * IExplodable Interface
 * @description: Interface welches Objekte Implementieren können,
 * welche explodieren können sollen.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
abstract class IExplodable {
  /**
   * Methode welches die Implementierung der Explosion einer [ExplodableEntity] enthalten muss
   * @param: [Entity] caller: Referenz auf das aufrufende Objekt, um Fallunterscheidungen zu erlauben.
   */ 
  void explode([Entity caller]);
}