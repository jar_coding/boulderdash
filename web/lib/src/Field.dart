part of boulderLib;
/**
 * Klasse zum Repräsentieren eines Feldes
 * @description: Die Fieldklasse repräsentiert die Belegung des Spielfelds
 * und lässt Abfragen und Veränderungsoperationen zu.
 * @author: Marcus Meiburg
 * @version: 1.0
 */
class Field {
  /**
   * Doppele Liste mit [Entity] Objekten welche das Spielfeld repräsentiert
   */
  List<List<Entity>>  _struct;
  /**
   * Prüfvariable ob das Feld das erste Mal gerendert werden soll oder nicht
   */
  bool _isInitialized;
  /**
   * Verbleibende Spielzeit des aktuellen Caves
   */
  num _remainingTime     = 0;
  /**
   * Benötigte Diamanten um den aktuellen Cave zu beenden
   */
  num _diamondsNeeded    = 0;
  /**
   * Benötigte Diamanten um Bonuspunkte zu erhalten
   */
  num _extraDiamondValue = 0;
  /**
   * Aktueller Punktestand
   */
  num _currentScore = 0;
  /**
   * Anzahl der bisher gesammelten Diamanten
   */
  num _currentDiamonds = 0;
  /**
   * Zeit bis die Amoeba stirbt
   */
  num _amoebaTime        = 0;
  /**
   * Zeit bis die Magicwall deaktiviert wird
   */
  num _magicwallTime     = 0;
  /**
   * Farbschema des Feldes
   */
  String _color          = "";
  /**
   * Gibt an ob das aktuelle Level beendet wurde
   */
  bool _isCaveDone = false;
  /**
   * Liste aller aktiven Amoeba objekte auf dem Feld
   */
  List<Amoeba>   _amoeba = new List<Amoeba>();

  Field.Empty();
  Field(this._struct,this._diamondsNeeded,this._extraDiamondValue,this._remainingTime);

  /* Getter */
  List<List<Entity>> get struct => this._struct;
  List<Amoeba> get amoeba       => this._amoeba;
  int get diamondsNeeded        => this._diamondsNeeded;
  int get extraDiamondValue     => this._extraDiamondValue;
  num get currentDiamonds       => this._currentDiamonds;
  num get currentScore          => this._currentScore;
  int get remainingTime         => this._remainingTime;
  int get amoebaTime            => this._amoebaTime;
  int get magicwallTime         => this._magicwallTime;
  String get color              => this._color;
  bool get isCaveDone       => this._isCaveDone;
  bool get isInitialized        => this._isInitialized;

  /* Setter */
  set currentDiamonds(num value)  { this._currentDiamonds = value; }
  set currentScore(num value)     { this._currentScore = value; }
  set remainingTime(int value)    { this._remainingTime = value; }
  set amoebaTime(num value)       { this._amoebaTime = value; }
  set magicwallTime(num value)    { this._magicwallTime = value; }
  set isInitialized(bool b)       { this._isInitialized = b; }
  set isCaveDone(bool b)          { this._isCaveDone = b; }

  /**
   * Initialisiert das Feld
   * @param: [List] field: aktuelle Feldbelegung
   * @param: [num] rT: verbleibene Zeit
   * @param: [num] dN: benötigte Diamanten
   * @param: [num] edV: Bonuspunkte
   * @param: [num] aT: Zeit bis die Amoeba stirbt
   * @param: [num] mT: Zeit bis die Magicwall deaktiviert wird
   * @param: [String] color: Farbschema
   * @param: [bool] init: Initailisierungsvariable (Bei True, wird das Feld komplett neugezeichnet)
   */
  void initialize(List<List<Entity>> field, num rT,num dN, num edV, num aT, num mT, String color, bool init) {
    this._struct = field;
    this._remainingTime = rT;
    this._diamondsNeeded = dN;
    this._extraDiamondValue = edV;
    this._amoebaTime = aT;
    this._magicwallTime = mT;
    this._color = color;
    this._isInitialized = init;
  }

  /**
   * Gibt die [Entity] an der Stelle x,y der [Field._struct] zurück
   * @param: num x -> X Wert der gesuchten [Entity]
   * @param: num y -> Y Wert der gesuchten [Entity]
   * @return: [Entity] an der Stelle x,y
   */
  Entity getEntityAt(num x, num y) {
    return this._struct[y][x];
  }

  /**
   * Ersetzt die [Entity] an der Position x,y durch eine neue [Entity]
   * Und setzt die neue [Entity] auf [Entity.isModified]
   * @param: [num] x: X-Wert der alten [Entity]
   * @param: [num] y: Y-Wert der alten [Entity]
   * @param: [Entity] e: neue [Entity]
   */
  void setEntityAt(num x, num y, Entity e) {
    this._struct[y][x] = e;
    e.isModified = true;
  }
  /**
   * Ersetzt die [Entity] in Richtung [dir] des [caller]s durch eine neue [Entity]
   * @param: [Entity] caller: Aufrufende [Entity]
   * @param: [num] dir: gewünschte Richtung
   * @param: [Entity] e: neue [Entity]
   */
  void setEntityInDir(Entity caller, num dir, Entity e) {
    this.setEntityAt(caller.x+DIRX[dir], caller.y+DIRY[dir], e);
  }
  /**
   * Gibt die [Entity] in Richtung [dir] der aufrufende [Entity] zurück
   * @param: [Entity] e: Aufrufende [Entity]
   * @param: [num] dir: Richtung in welche geprüft werden soll.
   * @return: [Entity] in Richtung [dir].
   */
  Entity getEntityInDir(Entity e, num dir) {
    return this.getEntityAt(e.x+DIRX[dir], e.y+DIRY[dir]);
  }
  /**
   * Ersetzt an der Stelle die Referenz auf [Entity] durch ein neues [Space] Objekt.
   * @param: [Entity] e: Referenz auf die auszutauschende [Entity].
   */
  void clearEntityAtPos(Entity e) {
    this.setEntityAt(e.x, e.y, new Space(e.x,e.y,this));
  }
  /**
   * Prüft ob in Richtung [dir] der [Entity] ein freies Feld ist (SPACE)
   * @param: [Entity] e: aufrufende Entity
   * @param: [num] dir: Richtung in die geprüft werden soll
   * @return: true -  falls in der Richtung ein [Space] Objekt liegt andernfalls false
   */
  bool isFree(Entity e,num dir) {
    return this.getEntityAt(e.x+DIRX[dir], e.y+DIRY[dir]) is Space;
  }
  /**
   * Prüft ob in Richtung [dir] der [Entity] ein Erdfeld liegt (DIRT)
   * @param: [Entity] e: aufrufende Entity
   * @param: [num] dir: Richtung in die geprüft werden soll
   * @return: true -  falls in der Richtung ein [Dirt] Objekt liegt andernfalls false
   */
  bool isDirt(Entity e, num dir) {
    return this.getEntityAt(e.x+DIRX[dir], e.y+DIRY[dir]) is Dirt;
  }

  /**
   * Prüft ob sich in allen vier möglichen Laufichtungen die [Entity] mit dem Typen [type] befindet
   * @param: [Entity] e: Aufrufende [Entity]
   * @param: [num] type: gesuchter Type
   * @return: true - Wenn sich in einer der Richtungen die [Entity] mit dem Typen [type] befindet, sonst false
   */
  bool isEntityArround(Entity e, num type) {
    return getEntityInDir(e, UP).type == type ||
           getEntityInDir(e, RIGHT).type == type ||
           getEntityInDir(e, DOWN).type == type ||
           getEntityInDir(e, LEFT).type == type;
  }

  /**
   * Ruft bei allen Updatebaren Entities die update Methode auf
   * und überpüft die Laufzeiten der [Amoeba] und der [Magicwall] Objekte
   */
  void update() {
    for(List<Entity> eList in this._struct.reversed) {
      for(Entity e in eList.reversed) {
        if(e is IUpdateable) {
          IUpdateable uE = e as IUpdateable;
          uE.update();
        }
      }
    }
    if(this._remainingTime <= this._amoebaTime && !Amoeba.isDead) {
      Amoeba.isDead = true;
    } else if(this._remainingTime <= this._magicwallTime && Magicwall.isActive &&!Magicwall.isDisabled) {
      Magicwall.isDisabled = true;
    }
  }
  /**
   * Erhöht die gesammelten Diamaten und den aktuellen Punktestand
   */
  void incDiamonds() {
    if(this._currentDiamonds < this.extraDiamondValue) {
      this._currentScore += DIAMOND_VALUE;
    } else {
      this._currentScore += EXTRA_DIAMOND_VALUE;
    }
    this._currentDiamonds++;
  }

}