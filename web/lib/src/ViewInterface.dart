part of boulderLib;

/**
 * ViewInterface Klasse
 * @description: Hilfsklasse welche die Daten des Models für die View aufarbeitet 
 * @author: Marcus Meiburg
 * @version: 1.0
 */
class ViewInterface {
  /**
   * Referenz auf das [Model]
   */ 
  Model _model;
  /**
   * Konstruktor um ein [ViewInterface] Objekt zu erstellen.
   */ 
  ViewInterface(this._model);

  /**
   * Prüft ob das aktuelle Spielfeld im initialisierungs Modus ist
   */ 
  bool get isInitialized => this._model.field.isInitialized;

  /**
   * Gibt den aktuellen Farbcode des Spielfeldes zurück
   */ 
  String get caveColor => this._model.field.color;
  /**
   * Gibt die aktuelle Cave Nummer zurück
   */ 
  String get cave => this._model._currentCaveNr.toString();
  /**
   * Berechnet die Feldbreite Anhand der Breite des Spielfeldes multipliziert mit der Block Größe einer Entity
   */ 
  String get fieldWidth => (this._model.field.struct.first.length * BLOCK_SIZE).toString();
  /**
   * Berechnet die Feldhöhe Anhand der Höhe des Spielfeldes multipliziert mit der Block Größe einer Entity
   */ 
  String get fieldHeight => (this._model.field.struct.length * BLOCK_SIZE).toString();
  /**
   * Das Gameoverbanner befindet sich auf der hälfte der Feldhöhe
   */ 
  String get gameoverPosition => (this._model.field.struct.length * BLOCK_SIZE / 2).toString();

  /**
   * Gibt den Extra Diamond Wert zweistellig als String zurück, wenn der Wert einstellig ist, wird vorne eine Null angehängt
   */ 
  String get extraDiamondValue => this._addLeftSomeZeros(this._model.field.extraDiamondValue, 2);
  /**
   * Gibt die aktuellen benötigten Diamanten zweistellig als String zurück, wenn der Wert einstellig ist, wird vorne eine Null angehängt
   */ 
  String get diamondsNeeded => this._addLeftSomeZeros(this._model.field.diamondsNeeded, 2);
  /**
   * Gibt die aktuellen Diamanten zweistellig als String zurück, wenn der Wert einstellig ist, wird vorne eine Null angehängt
   */ 
  String get currentDiamonds => this._addLeftSomeZeros(this._model.field.currentDiamonds, 2);
  /**
   * Gibt die aktuellen Punktestand sechstellig als String zurück, wenn der Wert nicht sechstellig ist, wird vorne mit Nullen aufgefüllt
   */ 
  String get currentScore => this._addLeftSomeZeros(this._model.field.currentScore, 6);
  /**
   * Gibt die aktuellen Leben zweistellig als String zurück, wenn der Wert einstellig ist, wird vorne eine Null angehängt
   */ 
  String get lifes => this._addLeftSomeZeros(this._model.life, 2);
  /**
   * Gibt die aktuelle verbleibende Zeit dreistellig als String zurück, wenn der Wert nicht dreistellig ist, wird vorne mit Nullen aufgefüllt
   */ 
  String get remainingTime => this._addLeftSomeZeros(this._model.field.remainingTime, 3);

  /**
   * Prüft ob der Status des Model auf isRunning ist.
   */
  bool get gameover => !this._model.isRunning;

  /**
   * Erstellt aus dem Spielfeld eine html tabellen Repräsentation
   */ 
  String get getField {
    this._model.field.isInitialized = false;
    final td = (Entity e) => "<td x='${e.x}' y='${e.y}' class='${typeMap.keys.elementAt(e.type)}'></td>";
    final tr = (row) => "<tr>" + row.map(td).join() + "</tr>";
    return "<table id='field_table'>${this._model.field.struct.map(tr).join()}</table>";
  }
  
  /**
   * Gibt eine Liste von Maps zurück welche abhängig von den modifizierten [Entity] Objekten 
   * den querySelector String und den Css ClassName enthalten.
   * @return: Liste mit den modifizierten [Entity]Daten als Map
   */ 
  List<Map<String,String>> get getModifiedMap {
    List<Map> lMap = new List<Map>();
    
    for (List<Entity> eL in this._model.field.struct) {
      for (Entity e in eL) {
        if (e.isModified) {
          
          Map<String,String> map = {
          "querySelector" : "#field_table td[x='${e.x}'][y='${e.y}']",
          "className"     : typeMap.keys.elementAt(e.type)
          };
          lMap.add(map);
          e.isModified = false;
        }
      }
    }
    return lMap;
  }

  /**
   * Fügt [value] vorne [len] Nullen an und gib das Ergebniss als String zurück
   * @param: [num] value: Übergebene Zahl
   * @param: [num] len: Länge auf die aufgefüllt werden soll.
   * @return: [len]Nullen+[value] als String
   */ 
  String _addLeftSomeZeros(num value, num len) {
    String result = value.toString();
    while (result.length < len) {
      result = "0" + result;
    }
    return result;
  }
}
