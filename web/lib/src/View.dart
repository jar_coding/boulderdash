part of boulderLib;
/**
 * View Klasse
 * @description: Der View ist das Element unseres Spiels,
 * dass für die optischen Anforderungen zuständig ist.
 * Alles was der Benutzer mit dem Auge wahrnehmen kann wird von ihm dargestellt.
 * @author: Marcus Meiburg
 * @version: 1.0
 */
class View {

  /* Referenzen auf die einzelnen HTML Elemente */
  Element htmlContent             = querySelector("#content");

  Element htmlFieldTable          = querySelector("#field_table");
  Element htmlGameOver            = querySelector("#field_gameover");
  Element htmlOverlay             = querySelector("#field_overlay");

  Element htmlCurrendDiamonds     = querySelector("#scoreboard_currendDiamonds");
  Element htmlScore               = querySelector("#scoreboard_score");
  Element htmlTime                = querySelector("#scoreboard_time");
  Element htmlDiamondsNeeded      = querySelector("#scoreboard_diamondsNeeded");
  Element htmlExtraDiamonds       = querySelector("#scoreboard_extraDiamonds");
  Element htmlLife                = querySelector("#scoreboard_life");

  Element htmlStartscreen         = querySelector("#startscreen");
  Element htmlCave                = querySelector("#startscreen_cave");
  Element htmlField               = querySelector("#field");
  /**
   * Referenz zur Hilfsklasse um die Daten aus dem Model zu bekommen
   */
  ViewInterface _viewInterface;
  /**
   * Konstruktor zum erstellen eines [View] Objekts, welcher eine Referenz auf ein [ViewInterface] Objekt übergeben bekommt
   * @param: [ViewInterface]: Referenz auf ein [ViewInterface] Objekt
   */
  View(this._viewInterface);

  /**
   * Setzt die Css Styles so, das der Startbildschirm zu sehen ist
   * und das Spielfeld ausgeblendet
   */
  void showStartscreen() {
    this.htmlStartscreen.style.height = "350px";
    this.htmlStartscreen.style.padding = "20px";
    this._showElement(this.htmlStartscreen);

    this._hideElement(this.htmlField);
    this.htmlFieldTable.innerHtml = "";
    this.htmlContent.style.width = "640px";

    this._hideGameover();
  }

  /**
   * Lässt das Gameover Banner verschwinden
   */
  void _hideGameover() {
    this.htmlGameOver.style.visibility = "hidden";
  }

  /**
   * Setzt das Spielfeld auf sichtbar
   */
  void showField() {
    this._hideGameover();
    this._showElement(this.htmlField);
    this.htmlField.style.height = "auto";
  }

  /**
   * Verändert die Sichtbarkeit des übergebenen Elements auf "sichtbar"
   * @param: [Element] e: Element welches sichbar werden soll
   */
  void _showElement(Element e) {
    e.style.visibility = "visible";
  }
  /**
   * Lässt das übergebenen Element verschwinden
   * @param: [Element] e: Element welches unsichtbar werden soll
   */
  void _hideElement(Element e) {
    e.style.visibility = "hidden";
    e.style.height = "0px";
    e.style.padding = "0";
  }

  /**
   * Zeichnet den Startbildschirm neu
   */
  void render_startscreen() {
    this.htmlCave.innerHtml = this._viewInterface.cave;
  }
  /**
   * Initialisierung des Feldes (beim ersten Start eines neuen Levels)
   */
  void render_initialize() {
    this._hideGameover();
    this._showElement(this.htmlField);
    this.htmlField.style.height = "auto";

    this._hideElement(this.htmlStartscreen);
    this.showField();

    String width = this._viewInterface.fieldWidth;
    String heigth = this._viewInterface.fieldHeight;

    this.htmlContent.style.width = width;

    this.htmlGameOver.style.width = width;
    this.htmlGameOver.style.top = this._viewInterface.gameoverPosition;

    this.htmlFieldTable.style.width = width;
    this.htmlFieldTable.style.height = heigth;
    this.htmlFieldTable.setInnerHtml(this._viewInterface.getField,validator: _htmlValidator);

    this.htmlOverlay.style.background = this._viewInterface.caveColor;
    this.htmlOverlay.style.width = width;
    this.htmlOverlay.style.height = heigth;

    this.htmlExtraDiamonds.setInnerHtml(this._viewInterface.extraDiamondValue.toString());
    this.htmlDiamondsNeeded.setInnerHtml(this._viewInterface.diamondsNeeded);
  }

  /**
   * Kontinuierliches Zeichnen der veränderten Werte und Felder
   */
  void render() {
    this._listToHtml();
    this.htmlLife.setInnerHtml("♥ ${this._viewInterface.lifes}");
    this.htmlCurrendDiamonds.setInnerHtml(this._viewInterface.currentDiamonds);
    this.htmlTime.setInnerHtml(this._viewInterface.remainingTime);
    this.htmlScore.setInnerHtml(this._viewInterface.currentScore);
    if(this._viewInterface.gameover) {
      this._showElement(this.htmlGameOver);
    }
  }

  /**
   * Berechnung der Entities welche neu gezeichnet werden müssen
   */
  void _listToHtml() {
    for(Map map in this._viewInterface.getModifiedMap) {
      // ["#field td[x='#NUMBER'][y='#NUMBER']","#CLASSNAME"]
      querySelector(map["querySelector"]).className = map["className"];
    }
  }

}