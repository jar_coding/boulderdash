part of boulderLib;
/**
 * Klasse zum repräsentieren eines einfachen [Explode] Blocks
 * @description: Der [Explode] Block entsteht wenn ein Lebewesen stirbt,
 * er gibt die Animation vor und das Verhalten welches nach der Explosion
 * erfolgen soll
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Explode extends Entity implements IUpdateable {
  
  /**
   * Ersteller des [Explode] Blocks
   */ 
  Entity _caller;
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [Entity] auf
   */   
  Explode(num x, num y, Field f, this._caller) : super(x, y, EXPLODE, f);

  num _frame = 0;
  num _maxFrame = 10;
  
  /**
   * Prüft ob sich die Entity in diesem Frame bewegen darf
   * @return: Entity kann sich bei true bewegen, bei false nicht.
   */
  bool _isReady() {
    this._frame++;
    if(this._frame >= this._maxFrame) {
      this._frame = 0;
      return true;
    }
    return false;
  }
  
  /**
   * Prüft nach der Abgelaufenen Frame Zeit anhand des callers, ob
   * sich die [Explode] Blöcke in [Diamond] verwandeln oder in [Space]
   */ 
  void update([dir]) {
    if(!this._isReady()) {
      return;
    }
    if(this._caller is Butterfly) {
      this._field.setEntityAt(this.x, this.y, new Diamond(this.x,this.y,this.field));
    } else {
      this.field.clearEntityAtPos(this);
    }
  }
}