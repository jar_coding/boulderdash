part of boulderLib;
/**
 * Klasse zum repräsentieren des Gegnertyps: [Amoeba]
 * @description: Die [Amoeba] kann sich zufällig in einer der vier möglichen
 * Laufrichtungen ausbreiten, falls sie sich nicht mehr aufbreiten kann
 * verwandelt sie sich in Diamanten, sollte sie jedoch ihre levelabhängige
 * Lebenszeit überschreiten verwandelt sie sich in Stein.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Amoeba extends MoveableEntity implements IUpdateable {
  
  bool _isEnclosed = false;
  static bool isDead = false;
  num _maxFrame = 50;
  
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [MoveableEntity] auf
   */ 
  Amoeba(int x, int y, Field f) : super(x, y, AMOEBA, f);

  /**
   * Gibt anhand einer Liste eine mögliche zufällige Richtung zurück
   * @param: [List] list: Liste von der ein zufälliges Element ausgesucht werden soll
   * @return: Zufällige Richtung 
   */ 
  num randomDir(List list) => list[new Random().nextInt(list.length)];
  
  /**
   * Prüft ob alle Teile der [Amoeba] eingeschlossen sind
   * @return: true - Wenn alle Teile eingeschlossen sind, sonst false
   */ 
  bool _amoebaIsEnclosed() {
    return this.field.amoeba.isEmpty;
  }
  
  /**
   * Anders als andere Objekte breitet sich die [Amoeba] aus, dass heißt
   * immer wenn sie sich "bewegt" wird sie größer und erschafft an einer
   * anliegenden freien Position ein neues [Amoeba] Objekt
   * @param: [num] dir: Richtung in die sich die [Amoeba] ausbreiten möchte
   * @return: true - wenn die Aktion erfolgreich war, sonst false
   */ 
  bool move(num dir) {
    if(this.field.isFree(this, dir) || this.field.isDirt(this, dir)) {
      int newX = this.x+DIRX[dir];
      int newY = this.y+DIRY[dir];
      Amoeba newAmoeba = new Amoeba(newX,newY,this.field);
      /* Fügt das neue Amoeba Segment der amoeba Liste in Field hinzu */
      this.field.amoeba.add(newAmoeba);
      this.field.setEntityAt(newX, newY, newAmoeba);
      return true;
    }
    return false;
  }
  
  /**
   * Prüft ob die Amoeba mit etwas zusammenstößt
   * @param: [num] dir: Richtung in die geprüft wird
   * @return: true - wenn es keinen Zusammenstoß gibt, sonst false
   */
  bool _canPass(num dir) {
    Entity e = this.field.getEntityInDir(this, dir);
    return e.type == SPACE || e.type == DIRT;
  }
  
  /**
   * Berechnet die Aktionen der Amoeba, die Zusammenstöße,
   * Aktionen beim Tot und ob sie eingeschlossen ist.
   */ 
  void update() {
    /* Wenn alle Teile eingeschlossen sind dann ohne Verzögerung Updaten */
    if(!this._amoebaIsEnclosed()) {
      if(!this._isReady()) {
        return;
      } 
    }
    /* Wenn alle Teile eingeschlossen sind oder die Amoeba tot ist */
    /* werden alle Teile entweder in einen Diamanten oder in ein Stein verwandelt */
    if(this._amoebaIsEnclosed() || Amoeba.isDead) {
      this.field.setEntityAt(this.x, this.y, Amoeba.isDead ? new Boulder(this.x,this.y,this.field) : new Diamond(this.x,this.y,this.field));
      return;
    }
    
    /* Wenn die Amoeba nicht geblockt ist breitet sie sich aus */
    if(this.field.isEntityArround(this, SPACE) ||
       this.field.isEntityArround(this, DIRT)) {
      if(this._isEnclosed) {
        this.field.amoeba.add(this);
        this._isEnclosed = false;
      }
      this.move(randomDir(moveableDir));
    /* Wenn die Amoeba eingeschlossen ist wird sie aus der [amoeba] Liste entfernt */
    } else if(!this._isEnclosed) {
      this._isEnclosed = true;
      this.field.amoeba.remove(this);
    }
  }
}