part of boulderLib;
/**
 * Klasse zum repräsentieren eines einfachen [Metal] Blocks
 * @description: [Metal] umrandet jedes Spielfeld, es kann weder
 * Explodieren oder sich bewegen.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Metal extends Entity{
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [Entity] auf
   */ 
  Metal(int x, int y, Field f) : super(x, y, METAL, f);
}