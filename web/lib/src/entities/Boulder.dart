part of boulderLib;
/**
 * Klasse zum repräsentieren eines Boulders
 * @description: Haupthinderniss im Spiel sind die Boulder(Steine)
 * welche zu einer gefahr für Rockford werden können.
 * Sie können horizontal fallen und bei abgerundeten Untergrund
 * zu Steinlawienen werden und Rockford zerquetschen.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Boulder extends MoveableEntity implements IUpdateable {

  num _maxFrame = 4;
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [MoveableEntity] auf
   */ 
  Boulder(int x, int y, Field f) : super(x, y, BOULDER, f);

  /**
   * Berechnung von Kollisionen und Bewegung des [Boulder] finden hier statt
   */ 
  void update() {
    if(!this._isReady()) {
      return;
    }
    
    Entity eBelow = this.field.getEntityInDir(this, DOWN);

    if(this.field.isFree(this,DOWN)) {
      this.move(DOWN);
      this._isMoving = true;
    } else if(this.field.isFree(this,LEFT) && this.field.isFree(this,DOWNLEFT) && isRounded(eBelow)) {
      this.move(LEFT);
      this._isMoving = false;
    } else if(this.field.isFree(this,RIGHT) && this.field.isFree(this,DOWNRIGHT)  && isRounded(eBelow)) {
      this.move(RIGHT);
      this._isMoving = false;
    } else if(this._isMoving && isLivingEntity(eBelow)) {
      if(eBelow is ExplodableEntity) {
        eBelow.explode();
      }
      this._isMoving = false;
    } else if(eBelow is Magicwall && !Magicwall.isActive && this._isMoving) {
      Magicwall.isActive = true;
      this.field.magicwallTime = this.field.remainingTime - this.field.magicwallTime;
    } else {
      this.lastDir = NONE;
      this._isMoving = false;
    }
  }
}