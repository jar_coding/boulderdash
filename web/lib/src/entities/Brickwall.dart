part of boulderLib;
/**
 * Klasse zum repräsentieren einer [Brickwall]
 * @description: Die Brickwall versperrt Rockford den Weg,
 * wenn du jedoch ein [Firefly] oder [Butterfly] in ihrer Nähe explodiert,
 * wird sie zerstört.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Brickwall extends ExplodableEntity {
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [ExplodableEntity] auf
   */ 
  Brickwall(int x, int y, Field f) : super(x, y, BRICKWALL, f);
}