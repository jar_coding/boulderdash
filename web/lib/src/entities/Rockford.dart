part of boulderLib;
/**
 * Klasse zum repräsentieren eines einfachen [Rockford] Blocks
 * @description: Rockford repräsentiert den Spieler auf dem Spielfeld,
 * er kann durch die Pfeiltasten bewegt werden und von Steinen und Diamanten
 * zerquetscht werden. Diamanten kann er zusätzlich noch sammeln um Punkte zu sammeln.
 * Steine wiederrum kann er in horizontaler Linie hin und herbewegen.
 * Wenn er auf Gegner wie die Butterfly oder den Firefly trifft, explodiert er.
 * @author: Marcus Meiburg
 * @version: 1.0
 */
class Rockford extends MoveableEntity {

  /**
   * 
   */ 
  bool _isDead = false;

  // Tempomodifikator
  num _maxFrame = 3;
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [MoveableEntity] auf
   */ 
  Rockford(int x, int y, Field f) : super(x, y, ROCKFORD, f);
  
  /* Getter */
  bool get isDead           => this._isDead;
  
  /* Setter */
  set isDead(bool b)              { this._isDead = b; }
  
  /**
   * Bewegt [Rockford] in eine gegebene Richtung
   * @param: [num] dir: Richtung in die sich[Rockford] bewegen soll
   * @return: true - Wenn die Bewegung erfolgreich war, sonst false
   */ 
  bool move(num dir) {
    if(!_isReady()) {
      return false;
    }   
    // Animationsbehandlung
    // Nach jedem Movevorgang soll die View die Position von Rockford updaten
    if(dir == NONE) {
      this.type = ROCKFORD;
    } else if(dir == LEFT) {
      this.type = ROCKFORD_LEFT;
    } else if(dir == RIGHT) {
      this.type = ROCKFORD_RIGHT;
    }
    this._isModified = true;
    return super.move(dir);
  }
  
  /**
   * Beim Tot von [Rockford] explodiert er und zerstört zusätzlich Blöcke rundrum.
   * @param: [Entity] caller: (OPTIONAL) Übergibt den aufrufer der Explodefunktion, für Fallunterscheidungen
   */ 
  void explode([Entity caller]) {
    super.explode();
    for(int dir = 0; dir < DIR_COUNT; dir++) {
      Entity e = this._field.getEntityInDir(this, dir);
      if(e is ExplodableEntity && !e.isExploded) {
        e.explode(this);
      }
    }
    this._isDead = true;
  }
  /**
   * Prüft ob [Rockford] sich in eine bestimmte Richtung bewegen kann.
   * @param: [num] dir: Richtung in die geprüft wird
   * @return: true - wenn es keine Kollision gab, sonst false
   */
  @override  
  bool _canPass(num dir) {  
    Entity e = this.field.getEntityInDir(this, dir);
    if(e.type == DIAMOND) {
      this.field.incDiamonds();
    } else if(e.type == OUTBOX_ACTIVE) {
      this.field.isCaveDone = true;
    } else if(e is Boulder) {
      if(horizontal(this.lastDir) /*&& new Random().nextInt(10).isOdd*/ && !e.isMoving) {
        return e.move(this.lastDir);
      }
    }
    return e.type == SPACE || e.type == DIRT || e.type == DIAMOND || e.type == OUTBOX_ACTIVE; 
  } 
}