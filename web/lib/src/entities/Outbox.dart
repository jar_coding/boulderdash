part of boulderLib;
/**
 * Klasse zum repräsentieren eines einfachen [Outbox] Blocks
 * @description: Die [Outbox] ist der Ausgang des Levels,
 * diese muss [Rockford] erreichen wenn er die benötigten
 * Diamanten gesammelt hat.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Outbox extends Entity implements IUpdateable {
  /**
   * Klassenvaribale, wodurch alle Ausgänge Global auf aktiv gesetzt werden können
   */ 
  static bool active = false;
  num _frame = 0;
  num _maxFrame = 1;
  
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [Entity] auf
   */ 
  Outbox(int x, int y, Field f) : super(x, y, OUTBOX, f);
  
  bool _isReady() => false;
  
  /**
   * Prüft ob die Outbox auf active gesetzt wurde, wenn ändert sich der typ
   * des Blocks auf OUTBOX_ACTIVE.
   */ 
  void update() {
    if(Outbox.active) {
      if(this.type == OUTBOX) {
        this._isModified = true;
        this.type = OUTBOX_ACTIVE;
      }
    }
  }
}