part of boulderLib;
/**
 * Klasse zum repräsentieren eines einfachen [Dirt] Blocks
 * @description: [Dirt] hindert Steine daran zu fallen, [Rockford]
 * kann sich jedoch hindurchgraben und die [Amoeba] darauf ausbreiten
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Dirt extends ExplodableEntity {
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [ExplodableEntity] auf
   */ 
  Dirt(int x, int y, Field f) : super(x, y, DIRT, f);
}