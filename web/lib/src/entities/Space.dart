part of boulderLib;
/**
 * Klasse zum repräsentieren eines einfachen [Space] Blocks
 * @description: [Space] kommt zum vorschein wenn [Rockford] 
 * sich durch [Dirt] gräbt oder wenn etwas explodiert.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Space extends ExplodableEntity {
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [ExplodableEntity] auf
   */ 
  Space(int x, int y, Field f) : super(x, y, SPACE, f);
}