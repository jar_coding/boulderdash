part of boulderLib;
/**
 * Klasse zum repräsentieren eines einfachen [Magicwall] Blocks
 * @description: [Magicwall] sieht aus wie eine [Brickwall],
 * wenn jedoch ein Stein oder ein Diamant hinauffällt,
 * wird sie aktiviert. Dadurch können Diamanten und Steine
 * hindurchfallen und werden von Steinen in Diamanten verwandelt
 * und andersherrum
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Magicwall extends ExplodableEntity implements IUpdateable {
  
  /**
   * Prüfvariable welche anzeigt ob die [Magicwall] schon deaktiviert ist
   */ 
  static bool isDisabled = false;
  /**
   * Prüfvariable welche alle [Magicwall] Objekte auf aktiv setzt
   */ 
  static bool isActive = false;
  num _maxFrame = 0;
  num _frame = 0;
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [ExplodableEntity] auf
   */   
  Magicwall(int x, int y, Field f) : super(x, y, MAGICWALL, f);
  
  /**
   * Prüft ob die Entity in diesem Frame ein Update ausführen darf
   * @return: Entity kann bei true updaten, bei false nicht.
   */
  bool _isReady() {
    this._frame++;
    if(this._frame >= this._maxFrame) {
      this._frame = 0;
      return true;
    }
    return false;
  }
  
  /**
   * Wandelt Steine die oberhalb der Magicwall sind in Diamanten und andersherrum
   */ 
  void update() {
    if(!this._isReady() || !Magicwall.isActive) {
      return;
    }

    if(Magicwall.isDisabled) {
      print("test");
      field.setEntityAt(this.x, this.y, new Brickwall(this.x,this.y,this.field));
      return;
    }
    
    if(this.type == MAGICWALL) {
      this._isModified = true;
      this.type = MAGICWALL_ACTIVE;
    }
    Entity eUpside = this.field.getEntityInDir(this, UP);
    Entity eBelow = this.field.getEntityInDir(this, DOWN);
    
    if(this.field.isFree(this, DOWN)) {
      if(eUpside is Boulder || eUpside is Diamond) {
        if(eUpside is MoveableEntity && eUpside.isMoving) {
          this.field.clearEntityAtPos(eUpside);
          this.field.setEntityAt(eBelow.x, eBelow.y, eUpside is Boulder ? new Diamond(eBelow.x,eBelow.y,this.field) : new Boulder(eBelow.x,eBelow.y,this.field));
        }
      }
    }
  }
}