part of boulderLib;
/**
 * Klasse zum repräsentieren des Gegnertyps: [Butterfly]
 * @description: Der [Butterfly] bewegt sich immer Links herrum,
 * wenn du ihn mit einem Stein triffst oder anderweitig eleminierst,
 * explodiert er und erschafft um sich herrum Diamanten
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Butterfly extends MoveableEntity implements IUpdateable {
  
  num _maxFrame = 4;
  /**
   * Konstruktor ruft den Konstruktor der Super Klasse [MoveableEntity] auf
   */   
  Butterfly(int x, int y, Field f) : super(x, y, BUTTERFLY, f);

  /**
   * Bewegt den [Butterfly] in eine gegebene Richtung
   * @param: [num] dir: Richtung in die sich der Butterfly bewegen soll
   * @return: true - Wenn die Bewegung erfolgreich war, sonst false
   */ 
  bool move(num dir) {
    if(!this._isReady()) {
      return false;
    }
    return super.move(dir);
  }
  
  /**
   * Beim Tot des [Butterfly] Objekts explodiert es und verwandelt alle angrenzenden Felder in Diamanten.
   * @param: [Entity] caller: (OPTIONAL) Übergibt den aufrufer der Explodefunktion, für Fallunterscheidungen
   */ 
  void explode([Entity caller]) {
    super.explode(this);
    for(int dir = 0; dir < DIR_COUNT; dir++) {
      Entity e = this._field.getEntityInDir(this, dir);
      if(e is ExplodableEntity && !e.isExploded) {
        e.explode(this);
      }
    }
  }
  
  /**
   * Updatefunktion die periodisch von [Field] aufgerufen wird
   */ 
  void update() {
    this._calcMoving(lastDir);
  }
  
  /**
   * Berechnung der Kollision und der Bewegung des [Butterfly] Objekts
   */ 
  void _calcMoving(num dir) {
    
    // Kollisionsabfrage
    if(this.field.isEntityArround(this, ROCKFORD) ||
       this.field.isEntityArround(this, AMOEBA)) {
      this.explode(this);
    }
    
    if(this.isBlocked) {
      return;
    } 
    var newDir = rotateRight(dir);

    if(this.field.isFree(this, newDir)) {
      this.move(newDir);
    } else if(this.field.isFree(this, dir)) {
      this.move(dir);
    } else {
      this._calcMoving(rotateLeft(dir));
    }
  }
  
  /**
   * Prüft ob [Butterfly] sich in eine bestimmte Richtung bewegen kann.
   * @param: [num] dir: Richtung in die geprüft wird
   * @return: true - wenn es keine Kollision gab, sonst false
   */
  @override
  bool _canPass(num dir) {
    Entity e = this.field.getEntityInDir(this, dir);
    if(e is Rockford) {
      e.explode();
      return false;
    } else if(e is Amoeba) {
      this.explode();
    } else if(this.field.isFree(this, dir)) {
      return true;
    }
    return false;
  }
}