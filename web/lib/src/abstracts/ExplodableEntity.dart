part of boulderLib;
/**
 * Klasse zum Repräsentieren eines explodierbaren Feldes
 * @description: Erbt von [Entity] und implemmtiert das Interface IExplodeable
 * wodurch es explodier bar wird.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
abstract class ExplodableEntity extends Entity implements IExplodable {
  /**
   * Flag, zeigt an ob das Objekt schon explodiert ist
   */ 
  bool _isExploded = false;
  
  /**
   * Der Konstruktor ruft den SuperKonstruktor der [Entity] Klasse auf
   */ 
  ExplodableEntity(int x, int y, int type, Field f) : super(x, y, type, f);
  
  /* Getter */
  bool get isExploded => this._isExploded;
  /* Setter */
  set isExploded(bool b) { this._isExploded = b; }
  
  @override
  void explode([Entity caller]) {
    this._isExploded = true;
    this.field.setEntityAt(this.x, this.y, new Explode(this.x ,this.y, this.field, caller));
  }
}