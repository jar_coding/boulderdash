part of boulderLib;
/**
 * Klasse zum Repräsentieren eines bewegbaren, explodierbaren Feldes
 * @description: Erbt von [ExplodeableEntity] und implemmtiert das Interface IMoveable,
 * wodurch es sich bewegen kann und explodieren.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
abstract class MoveableEntity extends ExplodableEntity implements IMoveable {
  num _frame = 0;
  num _maxFrame = 3;
  num _lastDir = NONE;
  bool _isMoving = false;
  
  /**
   * Konstruktor der MoveableEntity
   */
  MoveableEntity(int x, int y, int type, Field f) : super(x, y, type, f);
  
  /* Getter */
  num get frame => this._frame;
  num get maxFrame => this._maxFrame;
  num get lastDir => this._lastDir;
  bool get isMoving => this._isMoving;
  /* Setter */
  set frame(num value) { this._frame = value; }
  set maxFrame(num value) { this._maxFrame = value; }
  set lastDir(num value) { this._lastDir = value; }
  set isMoving(bool b) { this._isMoving = b; }
  
  /**
   * Prüft ob das aktuelle Frame die maximale Wartezeit für die [MoveableEntity]
   * überschritte hat. Und zählt dabei den _frame zähler hoch, bis er _maxFrame überschreitet.
   * @return: true - falls der Zähler schon überschritten wurde, sonst false 
   */ 
  bool _isReady() {
    this._frame++;
    if(this._frame >= this._maxFrame) {
      this._frame = 0;
      return true;
    }
    return false;
  }
  
  /**
   * Prüft ob in eine bestimmte Richtung kein Hinderniss ist.
   * @param: [num] dir: Richtung in die geprüft wird.
   */ 
  bool _canPass(num dir) {
    return this.field.isFree(this, dir);
  }
  
  /**
   * Entity ist geblockt wenn rundrum kein begehbares Feld ist.
   */
  bool get isBlocked => !this.field.isEntityArround(this, SPACE) && 
                        !this.field.isEntityArround(this, ROCKFORD);
 
  /**
   * Bewegt eine [Entity] in eine bestimmte freie Richtung
   * @params: dir: Richtung
   * @return: true, wenn die Bewegung erfolgreich war,
   * andernfalls false;
   */
  bool move(num dir) {
    this.lastDir = dir;
    if(this._canPass(dir)) {
      this.field.clearEntityAtPos(this);
      this.field.setEntityAt(this.x+DIRX[dir], this.y+DIRY[dir], this);
      this.x += DIRX[dir];
      this.y += DIRY[dir];
      return true;
    }
    return false;
  }
}