part of boulderLib;
/**
 * Klasse zum Repräsentieren eines einzelnen Feldes
 * @description: Beschreibt ein einzelnen Objekt auf dem Spielfeld,
 * mit welchem Interagiert werden kann.
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
abstract class Entity {
  /**
   * x und y Koordinate
   */ 
  num _x, _y;
  /**
   * Type der Entity
   */ 
  num _type = SPACE;
  /**
   * Referenz auf das Spielfeld
   */ 
  Field _field;
  /**
   * Flag ob die Entity kürzlich modifiziert/bewegt, oder der Typ geändert wurde
   */ 
  bool _isModified = true;

  /**
   * Konstruktor zum erstellen eines abgeleiteten [Entity] Objektes
   * @param: x: x-Koordinate;
   * @param: y: y-Koordinate;
   * @param: type: Type der Entity
   * @param: OPTIONAL: Referenz auf das Feld
   */ 
  Entity(this._x,this._y,this._type,[this._field]);

  /* Getter */
  int get x => this._x;
  int get y => this._y;
  int get type => this._type;
  Field get field => this._field;
  bool get isModified => this._isModified;
  /* Setter */
  set x(num x) { this._x = x; }
  set y(num y) { this._y = y; }
  set type(num t) { this._type = t; }
  set field(Field f) { this._field = f; }
  set isModified(bool b) { this._isModified = b; }
}