part of boulderLib;
/**
 * Controller Klasse
 * @description: Hier werden alle Eingaben des Spielers geprüft,
 * die Timer verwaltet und das Model und die View aufgerufen. 
 * @author: Marcus Meiburg
 * @version: 1.0
 */ 
class Controller {
  /**
   * Referenze auf die [Model] Klasse
   */ 
  Model _model;
  /**
   * Referenze auf die [View] Klasse
   */ 
  View  _view;
  /**
   * Variable für den letzten Tastendruck
   */ 
  num _inputDir = NONE;
  /**
   * Prüfvariable falls der Startbildschirm verlassen wurde
   */ 
  bool _isStarted = false;
  /**
   * Konstruktor um ein [Controller] Objekt zu erstellen.
   */ 
  Controller() {
    this._loadConfig();
  }
  /**
   * Methode zum Laden der Config-Daten aus der config.json Datei
   */
  void _loadConfig() {  
    HttpRequest.getString("config.json").then((String str) {
      config.addAll(JSON.decode(str));
    }).whenComplete(this._initialize); 
  }
  /**
   * Methode zum erstellen des Models, der View, zum initialisieren der KeyListener
   * und zum Starten des gameLoop Timers und des Clock Timers
   */
  void _initialize() {
    this._model = new Model();
    this._view  = new View(new ViewInterface(this._model));
    
    window.onKeyDown.listen((KeyboardEvent k) {
      switch(k.keyCode) {
        case KeyCode.UP:     this._inputDir = UP; break;
        case KeyCode.DOWN:   this._inputDir = DOWN; break;
        case KeyCode.LEFT:
          if(this._isStarted) {
            this._inputDir = LEFT;
          } else {
            this._model.decCurrentCaveNr();
            this._view.render_startscreen();
          }
          break;
        case KeyCode.RIGHT:
          if(this._isStarted) {
            this._inputDir = RIGHT;
          } else {
            this._model.incCurrentCaveNr();
            this._view.render_startscreen();
          }
          break;
        case KeyCode.ENTER:
          if(this._isStarted) {
              this._model.startNewGame(); 
          } else {
            this._isStarted = true;
            this._model.startNewGame();
            this._view.render_initialize();
          }
          break;
        case KeyCode.ESC:
          this._isStarted = false;
          this._model.isRunning = false;
          this._view.showStartscreen();
          break;
      }
      if(this._isStarted) {
        this._view.render();
      }
      // Verhindert das mitscrollen der Seite
      k.preventDefault();
    });
    window.onKeyUp.listen((KeyboardEvent k) {
      switch(k.keyCode) {
        case KeyCode.UP:     this._inputDir = this._inputDir == UP ? NONE : this._inputDir; break;
        case KeyCode.DOWN:   this._inputDir = this._inputDir == DOWN ? NONE : this._inputDir; break;
        case KeyCode.LEFT:   this._inputDir = this._inputDir == LEFT ? NONE : this._inputDir; break;
        case KeyCode.RIGHT:  this._inputDir = this._inputDir == RIGHT ? NONE : this._inputDir; break;
      }
    });
    
    /* Timer werden gestartet */
    new Timer.periodic(const Duration(milliseconds: PERIODIC_IN_MS), this._gameLoop);
    new Timer.periodic(const Duration(seconds: 1), this._clock);
  } 
  /**
   * Übergabe der Eingabeparameter der Tastatur an das Model
   */ 
  void _input() {
    this._model.movePlayer(this._inputDir);
  } 
  /**
   * Methode die periodisch vom Timer gestartet wird.
   * Falls das Spiel sich nicht mehr im Startbildschirm befindet
   * wird das Model angewiesen die decTime Methode aufzurufen.
   */ 
  void _clock(Timer _) {
    if(this._isStarted) {
      this._model.decTime();
    }
  }
  /**
   * Methode die periodisch vom Timer gestartet wird.
   * Falls das Spiel sich nicht mehr im Startbildschirm befindet
   * werden die Eingaben geprüft, das Model geupdatet und die View gerendert.
   */
  void _gameLoop(Timer _) {
    if(this._isStarted){
      this._input();
      this._model.update();
      if(this._model.field.isInitialized) {
        this._view.render_initialize(); 
      } else {
        this._view.render();
      }
    }
  }
}