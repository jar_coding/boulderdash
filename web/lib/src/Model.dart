part of boulderLib;
/**
 * Model Klasse
 * @description: Das Model ist für die Logik des Spiels zuständig,
 * es überwacht Spielabbrüche und berechnend zustände des Spielfeldes,
 * die dann vom ViewInterface abgefragt werden können.
 * @author: Marcus Meiburg
 * @version: 1.0
 */
class Model {
  /**
   * Referenziert die Spielfigur ([Rockford] Objekt)
   */
  Rockford _rockford = null;
  /**
   * Speichert die aktuelle Cave Nummer
   */
  int _currentCaveNr = 0;
  /**
   * Levelübergreifender Punktestand
   */
  int _totalScore = 0;
  /**
   * Leben des Spielers
   */
  int _life = 1;
  /**
   * Referenziert das aktuelle [Field] Objekt
   */
  Field _field = new Field.Empty();
  /**
   * Prüft ob das Spiel gerade im NeustartModus ist
   */
  bool _isRestarted = false;
  /**
   * Prüft ob das Spiel noch läuft.
   */
  bool _isRunning = false;
  /**
   * Referenz auf die Metadaten der Caves
   */
  List<Map> _caves = new List<Map>();
  /**
   * Konstruktor um ein [Model] Objekt zu erstellen.
   */
  Model() {
    this._loadCaves();
  }
  /**
   * Gibt eine Referenze auf das Spielfeld zurück
   */
  Field get field => this._field;
  /**
   * Gibt die aktuellen Leben des Spielers zurück
   */
  num get life => this._life;
  /**
   * Gibt zurück ob sich das Spiel gerade läuft
   */
  bool get isRunning => this._isRunning;
  /**
   * Gibt die aktuelle Levelnummer zurück
   */
  int get currentCaveNr => this._currentCaveNr;
  /**
   * Ändert den Laufstatus des Spieles
   */
  set isRunning(bool b) { this._isRunning = b; }
  /**
   * Übergibt der Spielfigur eine Richtung in der sie gehen soll,
   * falls sie nicht verstorben ist.
   * @param dir: Gibt die Richtung als nummerischen Wert an
   */
  void movePlayer(num dir) {
    if (this._isRunning && !this._rockford.isDead && !this._field.isCaveDone) {
      this._rockford.move(dir);
    }
  }
  /**
   * Dekrementiert die aktuelle Levelnummer um 1
   */
  void decCurrentCaveNr() {
    this.currentCaveNr = --this.currentCaveNr;
  }
  /**
   * Inkrementiert die aktuelle Levelnummer um 1
   */
  void incCurrentCaveNr() {
    this.currentCaveNr = ++this.currentCaveNr;
  }
  /**
   * Setzt das aktuelle Level auf den Wert [value] und püft ob er im gültigen Wertebereich liegt
   * @param: [num] value: neuer Wert auf den gesetzt werden soll.
   */
  set currentCaveNr(num value) {
    this._currentCaveNr = value < 0 ? this._caves.length - 1 : value >= this._caves.length ? 0 : value;
  }
  /**
   * Wird regelmäßig vom Controller aufgerufen und gibt Befehle an das Spielfeld weiter
   * und prüft Abbruchbedingungen
   */
  void update() {
    if (!this._isRunning) {
      return;
    }
    if (this._isRunning) {
      if (this._rockford.isDead && !this._isRestarted) {
        this._field.currentScore = 0;
        this._life--;
        if (this._life <= 0) {
          this._isRunning = false;
        } else {
          this._restart();
        }
      } else if (this._field._remainingTime <= 0) {
        this._field._remainingTime = 0;
        if (!this._isRestarted) {
          if (this._field.isCaveDone) {
            this._restart();
            this._field.isInitialized = true;
          } else {
            this._rockford.explode();
          }
        }
      } else if (!this._field.isCaveDone && this._field.currentDiamonds >= this._field._diamondsNeeded) {
        Outbox.active = true;
      } else if (this._field.isCaveDone) {
        this._addPoints();
      }
      this._field.update();
    }

  }
  /**
   * Dekrementiert die aktuelle Zeit um 1
   */
  void decTime() {
    if (this._isRunning) {
      this._field._remainingTime--;
    }
  }
  /**
   * Läd alle Caves aus den ".cave" Datein in die [_caves] Liste
   */
  void _loadCaves() {
    int i = 0;
    for (String cave in config["Caves"]) {
      this._caves.add(new Map());
      this._addCaves(cave,i++);
    }
  }
  /**
   * Dekodiert den JSON String eines [cave]s und fügt ihn an die [i]te Stelle der [_caves] Liste.
   * @param: [String] cave: Aktuelle Wertebelegung des Felds als String
   * @param: [num] i: Position an der das Cave in die Caveliste eingefügt werden soll
   */
  void _addCaves(String cave,num i) {
    HttpRequest.getString(cave).then((String str) {
      this._caves[i].addAll(JSON.decode(str));
    });
  }
  /**
   * Setzt die Initialwerte für ein neues Spiel
   */
  void startNewGame() {
    if (!this._isRunning) {
      this._life = LIFE_AT_THE_BEGIN;
      this._totalScore = 0;
      this._startGame();
    }
  }
  /**
   * Erstellt ein [Field] Objekt welches anhand von [_currentCaveNr]
   * aus der Liste [_caves] die dazugehörigen Daten läd.
   */
  void _startGame() {
    /* Übergibt dem Spielfeld alle relevanten Werte */
    this._field.initialize(this._createField(
        this._caves[this._currentCaveNr]["CaveStruct"]),
        this._caves[this._currentCaveNr]["Time"],
        this._caves[this._currentCaveNr]["DiamondsNeeded"],
        this._caves[this._currentCaveNr]["ExtraDiaValue"],
        this._caves[this._currentCaveNr]["AmoebaTime"],
        this._caves[this._currentCaveNr]["MagicwallTime"],
        this._caves[this._currentCaveNr]["CaveColor"],true);

    this._isRunning = true;
    this._isRestarted = false;
    this._field.currentScore = this._totalScore;
    this._field.currentDiamonds = 0;
    this._field.isCaveDone = false;
    this._rockford.isDead = false;
    Amoeba.isDead = false;
    Magicwall.isActive = false;
    Magicwall.isDisabled = false;
    Outbox.active = false;
  }
  /**
   * Setzt den Restartmodus des [Model] und
   * prüft ob das Level beendet wurde, falls nicht
   * wird das aktuelle Level nach einigen Sekunden neugeladen.
   */
  void _restart() {
    this._isRestarted = true;

    if (this._field.isCaveDone) {
      this.incCurrentCaveNr();
      this._life++;
    }
    this._totalScore += this._field.currentScore;
    new Timer(const Duration(seconds: SEC_TO_START), this._startGame);
  }
  /**
   * Berechened die Punkte abhängig von der noch verbleibenden Zeit beim Abschluss des Levels
   */
  void _addPoints() {
    this._field.currentScore += this._field.remainingTime;
    this._field.remainingTime = 0;
  }
  /**
   * Erstellt eine Liste aus der Startbelegung eines Caves,
   * anhand dieser werden Objekte erstellt, welche in die
   * Rückgabeliste gespeichert werden.
   * @param: list - Metadaten des Spielfeldes, welches die einzelnene Typen Belegungen enthält.
   * @return: Gibt eine Liste mit Entitys zurück.
   */
  List<List<Entity>> _createField(list) {
    List<List<Entity>> resultList = new List<List<Entity>>();
    for (int y = 0; y < list.length; y++) {
      List<Entity> entityList = new List<Entity>();
      final col = list.elementAt(y);
      for (int x = 0; x < col.length; x++) {
        Entity e = null;
        var type = col.elementAt(x);
        switch (type) {
          case AMOEBA:
            e = new Amoeba(x, y, this._field);
            this._field.amoeba.add(e);
            break;
          case BRICKWALL:
            e = new Brickwall(x, y, this._field);
            break;
          case BOULDER:
            e = new Boulder(x, y, this._field);
            break;
          case BUTTERFLY:
            e = new Butterfly(x, y, this._field);
            break;
          case DIAMOND:
            e = new Diamond(x, y, this._field);
            break;
          case DIRT:
            e = new Dirt(x, y, this._field);
            break;
          //case EXPLODE:   e = new Explode(x,y,this._currentField); break;
          case FIREFLY:
            e = new Firefly(x, y, this._field);
            break;
          case MAGICWALL:
            e = new Magicwall(x, y, this._field);
            break;
          case METAL:
            e = new Metal(x, y, this._field);
            break;
          case ROCKFORD:
            e = new Rockford(x, y, this._field);
            this._rockford = e;
            break;
          case SPACE:
            e = new Space(x, y, this._field);
            break;
          case OUTBOX:
            e = new Outbox(x, y, this._field);
            break;
        }
        entityList.add(e);
      }
      resultList.add(entityList);
    }
    return resultList;
  }
}
